galaxy-install:
	sudo ansible-galaxy install -r requirements.yaml --force -g

deploy-server:
	sudo ansible-playbook server.yaml

deploy-servers:
	ansible-playbook server.yaml -e env=openstack

git-update:
	sudo git pull

update-server: git-update galaxy-install deploy-server